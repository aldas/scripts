/**
 * @author    Takashi MATSUSHITA
 */

#ifndef MenuCut_hh
#define MenuCut_hh

/*=================================================================* 
 * declarations
 *=================================================================*/
/*-----------------------------------------------------------------* 
 * headers
 *-----------------------------------------------------------------*/
#include <vector>


// forward declarations
struct MenuObject;


// collections of "cuts"
namespace MenuCut
{

/** cut definitions */
enum CutDefinitions
{
  CHGCOR_OS = -1,         /**< opposite-sign */
  CHGCOR_SS =  1,         /**< same-sign */
  NO_QUALITY = 0xFFFF,    /**< no quality cut */
  MU_QLTY_SNGL = 0xF000,  /**< single muon quality cut */
  MU_QLTY_DBLE = 0xFF00,  /**< double muon quality cut */
  MU_QLTY_OPEN = 0xFFF0,  /**< open muon quality cut */
  NO_ISOLATION = 0xF,     /**< no isolation requirement */
};


/** type for look-up-table based operations */
enum LutType
{
  DeltaEta,     /**< delta-eta */
  DeltaPhi,     /**< delta-phi */
  CoshDeltaEta, /**< hyperbolic cosine */
  CosDeltaPhi   /**< hyperbolic sine */
};


/** type of precisions for fixed-point operations */
enum PrecisionType
{
  Delta,    /**< for delta-eta/phi/R */
  Mass,     /**< for invariant mass */
  MassPt,   /**< for pt */
  Math      /**< for hyperbolic cosine/sine */
};


const int MaxTaus = 8;  /**< number of maximum taus */


/** find iterator of an array
 *
 *  @param left [in] iterator pointing to left boundary of the array
 *  @param right [in] iterator pointing to right boundary of the array
 *  @param value [in] value to be inserted in the array
 *  @param getRight [in] returns right bounardy if true, otherwise returns left boundary
 *  @return left most iterator of the array which is smaller than or equal to the given value
 */
template<class RandomIt, class T> RandomIt
find_index(RandomIt left,
           RandomIt right,
           const T& value,
           bool getRight=false);


/** find index of an array
 *
 *  @param array [in] array of double
 *  @param value [in] value to be inserted in the array
 *  @param getRight [in] returns right bounardy if true, otherwise returns left boundary
 *  @return left most index of the array which is smaller than or equal to the given value
 */
size_t
findIndex(const std::vector<double>& array,
          const double value,
          size_t* index=0,
          bool getRight=false);


/**
 * structure for specifying range in double
 */
struct Range
{
  double minimum;
  double maximum;

  Range() : minimum(0.), maximum(0.) {}

  void clear()
  {
    minimum = 0.;
    maximum = 0.;
  }

#ifdef CEREAL_CEREAL_HPP_
template<class Archive>
void serialize(Archive& archive)
{
  archive(CEREAL_NVP(minimum),
          CEREAL_NVP(maximum));
}

void
print(const std::string& name,
      cereal::JSONOutputArchive& json)
{
  json(cereal::make_nvp(name, *this));
}
#endif
};


/**
 * structure for specifying range in integer
 */
struct IRange
{
  int minimum;
  int maximum;

  IRange() : minimum(0), maximum(0) {}

  void clear()
  {
    minimum = 0;
    maximum = 0;
  }

#ifdef CEREAL_CEREAL_HPP_
template<class Archive>
void serialize(Archive& archive)
{
  archive(CEREAL_NVP(minimum),
          CEREAL_NVP(maximum));
}

void
print(const std::string& name,
      cereal::JSONOutputArchive& json)
{
  json(cereal::make_nvp(name, *this));
}
#endif
};


/** get precision needed for fixed-point operation specified by the type */
int
getPrecision(const MenuObject& obj1,
             const MenuObject& obj2,
             const int type);

/** set look-up-tables for the type */
void
setConvLuts(const int type,
            const int* &lut_conv,
            const int* &lut_delta,
            int& nn,
            bool isEta=true);

/** get fixed-point delta-eta or hyperbolic-cosine of delta-eta */
long long
getDeltaEta(const MenuObject& obj0,
            const MenuObject& obj1,
            const int type);

/** get fixed-point delta-phi or hyperbolic-sine of delta-phi */
long long
getDeltaPhi(const MenuObject& obj0,
            const MenuObject& obj1,
            const int type);

/** get Et in fixed-point */
int
getEt(const MenuObject& object);



/**
 * class for cuts on object
 */
const int ObjectVersion = 1;
class Object
{
  public:
  // ctor
  Object() { clear(); }
  Object(int type, float threshold)
  {
    clear();
    this->type = type;
    this->threshold = threshold;
  }

  // dtor
  ~Object() {};

  int version;
  int type;
  int bx;
  float threshold;
  Range eta1, eta2;
  Range phi1, phi2;
  int quality;
  int isolation;
  int charge;
  int ithreshold;
  IRange ieta1, ieta2;
  IRange iphi1, iphi2;


  /** clear cuts */
  void clear();

  /** set cut values for firmware emulation */
  void setEmulationCut();

#ifdef CEREAL_CEREAL_HPP_
template<class Archive>
void serialize(Archive& archive)
{
  setEmulationCut();
  archive(CEREAL_NVP(version),
          CEREAL_NVP(type),
          CEREAL_NVP(bx),
          CEREAL_NVP(threshold),
          CEREAL_NVP(eta1),
          CEREAL_NVP(eta2),
          CEREAL_NVP(phi1),
          CEREAL_NVP(phi2),
          CEREAL_NVP(quality),
          CEREAL_NVP(isolation),
          CEREAL_NVP(charge),
          CEREAL_NVP(ithreshold),
          CEREAL_NVP(ieta1),
          CEREAL_NVP(ieta2),
          CEREAL_NVP(iphi1),
          CEREAL_NVP(iphi2));
}

void
print(const std::string& name,
      cereal::JSONOutputArchive& json)
{
  json(cereal::make_nvp(name, *this));
}
#endif

};


/**
 * class for cuts on correlation
 */
const int CorrelationVersion = 1;
class Correlation
{
  public:

  // ctor
  Correlation() { clear(); };

  // dtor
  ~Correlation() { };

  int version;
  Range deltaEta;
  Range deltaPhi;
  Range deltaR;
  Range mass;

  int charge_correlation;


  /** clear cuts */
  void clear();


  /** apply cuts on correlation
   *
   *  @return true if cuts are satisfied, false otherwise
   */
  bool select(const MenuObject& obj0, const MenuObject& obj1) const;

#ifdef CEREAL_CEREAL_HPP_
template<class Archive>
void serialize(Archive& archive)
{
  archive(CEREAL_NVP(version),
          CEREAL_NVP(deltaEta),
          CEREAL_NVP(deltaPhi),
          CEREAL_NVP(deltaR),
          CEREAL_NVP(mass),
          CEREAL_NVP(charge_correlation));
}

void
print(const std::string& name,
      cereal::JSONOutputArchive& json)
{
  json(cereal::make_nvp(name, *this));
}
#endif
};

} // namespace MenuCut


/** set indicies for objects satisfying the given cuts */
void
setCandidates(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>& data,
              std::vector<int>& candidates,
              const MenuCut::Object& cut);

#endif // MenuCut_hh
