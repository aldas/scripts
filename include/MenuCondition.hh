/**
 * @author    Takashi MATSUSHITA
 */

#ifndef MenuCondition_hh
#define MenuCondition_hh

/*=================================================================* 
 * declarations
 *=================================================================*/
/*-----------------------------------------------------------------* 
 * headers
 *-----------------------------------------------------------------*/
#include <memory>

#include "TTreeReaderValue.h"
#include "L1Trigger/L1TNtuples/interface/L1AnalysisL1UpgradeDataFormat.h"

#ifdef CEREAL_CEREAL_HPP_
#include <cereal/types/polymorphic.hpp>
#endif



// forward declarations
namespace MenuCut
{
  struct Object;
  struct Correlation;
}



// collections of "conditions"
namespace MenuCondition
{

class Condition
{
  public:
  enum Types {
    ObjectType,
    CorrelationType,
    OperatorType,
  };

  Condition() {};
  virtual ~Condition() {};

  Types type;
  

  virtual bool select(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>& data) = 0;

#ifdef CEREAL_CEREAL_HPP_
  virtual void print(const std::string& name, cereal::JSONOutputArchive& json) = 0;
#endif
};



/**
 * This class implements object condition
 */
class Object : public Condition
{
  public:

  Object() : cuts() { type = ObjectType; }; // ctor
  virtual ~Object() {}; // dtor

  std::vector<MenuCut::Object> cuts;  /**< cuts on objects */


  /** clear cuts */
  void clear() { cuts.clear(); }

  /** apply cuts on objects
   *
   *  @return true if cuts are satisfied, false otherwise
   */
  virtual bool select(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>& data);

  /** apply cuts on esum */
  bool esum(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>& data);

  /** apply cuts on single-object */
  bool single(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>& data);

  /** apply cuts on multi-object */
  bool multi(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>& data);


#ifdef CEREAL_CEREAL_HPP_
  template<class Archive>
  void serialize(Archive& archive)
  {
    archive(cuts);
  }

  virtual void
  print(const std::string& name,
        cereal::JSONOutputArchive& json)
  {
    json(cereal::make_nvp(name, cuts));
  }
#endif
};



/**
 * This class implements correlation condition
 */
class Correlation : public Condition
{
  public:

  MenuCut::Object objectCut0;   /**< cuts on object0 */
  MenuCut::Object objectCut1;   /**< cuts on object1 */
  MenuCut::Correlation correlationCut;  /**< cuts on values computed from the two objects */

  // ctor
  Correlation() : objectCut0(), objectCut1(), correlationCut() { type = CorrelationType; };
  Correlation(const MenuCut::Object& cut0, const MenuCut::Object& cut1, const MenuCut::Correlation& cut2)
    : objectCut0(cut0), objectCut1(cut1), correlationCut(cut2) { type = CorrelationType; };

  virtual ~Correlation() {};  // dtor


  /** clear cuts */
  void clear()
  {
    objectCut0.clear();
    objectCut1.clear();
    correlationCut.clear();
  }

  /** apply correlation conditions
   *
   *  @return true if conditions are satisfied, false otherwise
   */
  virtual bool select(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>& data);

#ifdef CEREAL_CEREAL_HPP_
  template<class Archive>
  void serialize(Archive& archive)
  {
    archive(cereal::make_nvp("obj1", objectCut0),
            cereal::make_nvp("obj2", objectCut1),
            cereal::make_nvp("correlation", correlationCut));
  }

  void
  virtual print(const std::string& name,
        cereal::JSONOutputArchive& json)
  {
    json(cereal::make_nvp(name, *this));
  }
#endif
};



class Operator : public Condition
{
  public:

  enum LogicalOperator
  {
    Undef,
    AND,
    OR,
    NOT
  };

  LogicalOperator logical_operator;

  Operator() : logical_operator(Undef) { type = OperatorType; };
  Operator(LogicalOperator x) : logical_operator(x) { type = OperatorType; };
  ~Operator() {};

  virtual bool select(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>& data) {return true;};

#ifdef CEREAL_CEREAL_HPP_
  template<class Archive>
  void serialize(Archive& archive)
  {
    archive(cereal::make_nvp("operator", logical_operator));
  }

  virtual void print(const std::string& name, cereal::JSONOutputArchive& json)
  {
    json(cereal::make_nvp(name, *this));
  };
#endif
};



class Composite
{
  public:

  Composite() {};
  ~Composite() {};

  std::vector<std::shared_ptr<Condition> > operands;

  bool select(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>& data);

#ifdef CEREAL_CEREAL_HPP_
  virtual void print(const std::string& name, cereal::JSONOutputArchive& json)
  {
    json(cereal::make_nvp(name, operands));
  };
#endif
};



/*-----------------------------------------------------------------* 
 * implementations
 *-----------------------------------------------------------------*/
bool
Object::select(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>& data)
{
  bool rc = false;
  size_t n = cuts.size();
  if (n == 1)
  {
    switch (cuts.at(0).type)
    {
      case L1Analysis::kTotalEt:
      case L1Analysis::kTotalHt:
      case L1Analysis::kMissingEt:
      case L1Analysis::kMissingHt:
      case L1Analysis::kMissingEtHF:
      case L1Analysis::kMinBiasHFP0:
      case L1Analysis::kMinBiasHFM0:
      case L1Analysis::kMinBiasHFP1:
      case L1Analysis::kMinBiasHFM1:
      case L1Analysis::kTotalEtEm:
      case L1Analysis::kTowerCount:
        rc = esum(data);
        break;
      case MenuObject::Muon:
      case MenuObject::Egamma:
      case MenuObject::Jet:
      case MenuObject::Tau:
        rc = single(data);
        break;
      default:
        throw std::runtime_error("Object::select: unknown type");
    }
  }
  else
  {
    rc = multi(data);
  }
  return rc;
}


bool
Object::esum(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>& data)
{
  MenuCut::Object& cut = cuts.at(0);
  MenuObject object(cut.type);
  for (size_t ii = 0; ii < data->sumType.size(); ii++)
  {
    if (data->sumType.at(ii) != cut.type) continue;
    object.set(data, ii);
    if (object.select(cut)) return true;
  }

  return false;
}


bool
Object::single(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>& data)
{
  MenuCut::Object& cut = cuts.at(0);

  std::vector<int> candidates;
  setCandidates(data, candidates, cut);

  MenuObject object(cut.type);
  for (size_t ii = 0; ii < candidates.size(); ii++)
  {
    object.set(data, candidates.at(ii));
    if (object.select(cut)) return true;
  }

  return false;
}


bool
Object::multi(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>& data)
{
  if (cuts.empty()) return false;

  std::vector<int> candidates;
  setCandidates(data, candidates, cuts.at(0));

  bool pass = false;
  const size_t nn = cuts.size();
  if (candidates.size() < nn) return pass;

  std::vector<std::vector<int> > combination;
  getCombination(candidates.size(), nn, combination);
  std::vector<std::vector<int> > permutation;
  getPermutation(nn, permutation);

  for (size_t ii = 0; ii < combination.size(); ii++)
  {
    const std::vector<int>& set = combination.at(ii);
    for (size_t jj = 0; jj < permutation.size(); jj++)
    {
      const std::vector<int>& indicies = permutation.at(jj);
      pass = true;
      for (size_t kk = 0; kk < nn; kk++)
      {
        int index = candidates.at(set.at(indicies.at(kk)));
        MenuObject object(cuts.at(kk).type);
        object.set(data, index);
        pass &= object.select(cuts.at(kk));
        if (not pass) break;
      }
      if (pass) break;
    }
    if (pass) break;
  }

  return pass;
}


bool
Correlation::select(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>& data)
{
  std::vector<int> candidates0, candidates1;
  setCandidates(data, candidates0, objectCut0);
  setCandidates(data, candidates1, objectCut1);

  bool pass = false;

  for (size_t ii = 0; ii < candidates0.size(); ii++)
  {
    MenuObject obj0(objectCut0.type);
    obj0.set(data, candidates0.at(ii));
    if (not obj0.select(objectCut0)) continue;

    for (size_t jj = 0; jj < candidates1.size(); jj++)
    {
      if ((ii == jj) and (objectCut0.type == objectCut1.type)) continue;
      MenuObject obj1(objectCut1.type);
      obj1.set(data, candidates1.at(jj));
      if (not obj1.select(objectCut1)) continue;

      pass = correlationCut.select(obj0, obj1);
      if (pass) break;
    }
    if (pass) break;
  }
  return pass;
}


bool
Composite::select(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>& data)
{
  std::stack<bool> stack;

  for (size_t ii = 0; ii < operands.size(); ii++)
  {
    std::shared_ptr<Condition> x = operands.at(ii);
    if (x->type == Condition::ObjectType or x->type == Condition::CorrelationType)
    {
      stack.push(x->select(data));
      continue;
    }

    if (x->type == Condition::OperatorType)
    {
      std::shared_ptr<Operator> op = std::dynamic_pointer_cast<Operator>(x);
      const Operator::LogicalOperator lop = op->logical_operator;
      switch (lop)
      {
        case Operator::AND:
        case Operator::OR:
          {
            if (stack.size() < 2) throw std::runtime_error("logic error a");
            bool l = stack.top();
            stack.pop();
            bool r = stack.top();
            stack.pop();
            if (lop == Operator::AND) stack.push(l and r);
            else stack.push(l or r);
          }
          break;

        case Operator::NOT:
          {
            if (stack.size() < 1) throw std::runtime_error("logic error b");
            bool b = stack.top();
            stack.pop();
            stack.push(not b);
          }
          break;

        case Operator::Undef:
          throw std::runtime_error("operator type undefined");
          break;
      }
      continue;
    }

    throw std::runtime_error("unknown operand");
  }

  if (stack.size() != 1) throw std::runtime_error("logic error c");
  return stack.top();
}

} // namespace MenuCondition


#ifdef CEREAL_CEREAL_HPP_
CEREAL_REGISTER_TYPE(MenuCondition::Object);
CEREAL_REGISTER_TYPE(MenuCondition::Condition);
CEREAL_REGISTER_TYPE(MenuCondition::Operator);
CEREAL_REGISTER_TYPE(MenuCondition::Correlation);
CEREAL_REGISTER_POLYMORPHIC_RELATION(MenuCondition::Condition, MenuCondition::Object);
CEREAL_REGISTER_POLYMORPHIC_RELATION(MenuCondition::Condition, MenuCondition::Condition);
CEREAL_REGISTER_POLYMORPHIC_RELATION(MenuCondition::Condition, MenuCondition::Operator);
CEREAL_REGISTER_POLYMORPHIC_RELATION(MenuCondition::Condition, MenuCondition::Correlation);
#endif

#endif // MenuCondition_hh
