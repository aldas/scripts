/**
 * @author    Takashi MATSUSHITA
 */

#ifndef MenuObject_hh
#define MenuObject_hh

/*=================================================================* 
 * declarations
 *=================================================================*/
/*-----------------------------------------------------------------* 
 * headers
 *-----------------------------------------------------------------*/
#include "TTreeReaderValue.h"
#include "L1Trigger/L1TNtuples/interface/L1AnalysisL1UpgradeDataFormat.h"


// forward declarations
namespace MenuCut
{
  struct Object;
}


/**
 * This class implements object for the menu
 */
class MenuObject
{
  public:

  /** type of object */
  enum ObjectType
  {
    Undef = -1,   /**< undefined */
    Muon = 10000, /**< muon */
    Egamma,       /**< egamma */
    Jet,          /**< jet */
    Tau           /**< tau */
  };

  // ctor
  MenuObject() { clear(); }
  MenuObject(int type)
  {
    clear();
    this->type = type;
  }

  // dtor
  ~MenuObject() {};


  int type;
  int bx;
  float et;
  float eta;
  float phi;
  int quality;
  int isolation;
  int charge;
  int iet;
  int ieta;
  int iphi;


  /** clear cuts */
  void clear();

  /** set objects for the data specified */
  void set(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>& data,
           const int index);

  /** apply cuts on object
   *
   *  @param cut [in] cuts on object
   *  @return true if cuts are satisfied, false otherwise
   */
  bool select(MenuCut::Object& cut);
};
#endif // MenuObject_hh
