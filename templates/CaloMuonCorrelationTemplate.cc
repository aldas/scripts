{#
 # @author: Takashi MATSUSHITA
 #}
{% block CaloMuonCorrelationTemplate scoped %}

{% import 'macros.jinja2' as macros %}

{% set objects = cond.getObjects() %}
{% set nObjects = objects | length %}

{% set calo = objects[0] if objects[0].getType() in (tmEventSetup.Egamma, tmEventSetup.Tau, tmEventSetup.Jet) else objects[1] %}
{% set muon = objects[1] if objects[1].getType() == tmEventSetup.Muon else objects[0] %}

{% if calo.getType() == tmEventSetup.Egamma %}
  {% set prefixCalo = prefixEg %}
  {% set nEtaBitsCalo = scaleMap[tmGrammar.EG + '-' + tmGrammar.ETA].getNbits() -%}
  {% set precision = 2*scaleMap['PRECISION-' + tmGrammar.EG + '-' + tmGrammar.MU + '-MassPt'].getNbits()
                   +   scaleMap['PRECISION-' + tmGrammar.EG + '-' + tmGrammar.MU + '-Math'].getNbits() %}
  {% set LUTS = {'ETA_OFFSET': 2**scaleMap[tmGrammar.EG + '-' + tmGrammar.ETA].getNbits(),
                 'CONV_ETA': 'LUT_ETA_EG2MU',
                 'CONV_PHI': 'LUT_PHI_EG2MU',
                 'DPHI': 'LUT_DPHI_EG_MU',
                 'DETA': 'LUT_DETA_EG_MU',
                 'COS_DPHI': 'LUT_COS_DPHI_EG_MU',
                 'COSH_DETA': 'LUT_COSH_DETA_EG_MU',
                 'ET0': 'LUT_EG_ET',
                 'ET1': 'LUT_MU_ET',
                 'PREC_MASS': precision
                } %}

{% elif calo.getType() == tmEventSetup.Jet %}
  {% set prefixCalo = prefixJet %}
  {% set nEtaBitsCalo = scaleMap[tmGrammar.JET + '-' + tmGrammar.ETA].getNbits() -%}
  {% set precision = 2*scaleMap['PRECISION-' + tmGrammar.JET + '-' + tmGrammar.MU + '-MassPt'].getNbits()
                   +   scaleMap['PRECISION-' + tmGrammar.JET + '-' + tmGrammar.MU + '-Math'].getNbits() %}
  {% set LUTS = {'ETA_OFFSET': 2**scaleMap[tmGrammar.JET + '-' + tmGrammar.ETA].getNbits(),
                 'CONV_ETA': 'LUT_ETA_JET2MU',
                 'CONV_PHI': 'LUT_PHI_JET2MU',
                 'DPHI': 'LUT_DPHI_JET_MU',
                 'DETA': 'LUT_DETA_JET_MU',
                 'COS_DPHI': 'LUT_COS_DPHI_JET_MU',
                 'COSH_DETA': 'LUT_COSH_DETA_JET_MU',
                 'ET0': 'LUT_JET_ET',
                 'ET1': 'LUT_MU_ET',
                 'PREC_MASS': precision
                } %}

{% elif calo.getType() == tmEventSetup.Tau %}
  {% set prefixCalo = prefixTau %}
  {% set nEtaBitsCalo = scaleMap[tmGrammar.TAU + '-' + tmGrammar.ETA].getNbits() -%}
  {% set precision = 2*scaleMap['PRECISION-' + tmGrammar.TAU + '-' + tmGrammar.MU + '-MassPt'].getNbits()
                   +   scaleMap['PRECISION-' + tmGrammar.TAU + '-' + tmGrammar.MU + '-Math'].getNbits() %}
  {% set LUTS = {'ETA_OFFSET': 2**scaleMap[tmGrammar.TAU + '-' + tmGrammar.ETA].getNbits(),
                 'CONV_ETA': 'LUT_ETA_TAU2MU',
                 'CONV_PHI': 'LUT_PHI_TAU2MU',
                 'DPHI': 'LUT_DPHI_TAU_MU',
                 'DETA': 'LUT_DETA_TAU_MU',
                 'COS_DPHI': 'LUT_COS_DPHI_TAU_MU',
                 'COSH_DETA': 'LUT_COSH_DETA_TAU_MU',
                 'ET0': 'LUT_JET_ET',
                 'ET1': 'LUT_TAU_ET',
                 'PREC_MASS': precision
                } %}
{% endif %}

{% set nEtaBitsMuon = scaleMap[tmGrammar.MU + '-' + tmGrammar.ETA].getNbits() -%}
{% set nPhiBitsMuon = scaleMap[tmGrammar.MU + '-' + tmGrammar.PHI].getNbits() -%}
{% set phiScaleMuon = scaleMap[tmGrammar.MU + '-' + tmGrammar.PHI] %}
{% set iPiMuon = (0.5*(phiScaleMuon.getMaximum() - phiScaleMuon.getMinimum())/phiScaleMuon.getStep()) | int -%}


bool
{{ cond.getName() }}
(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>& data)
{
  bool pass = false;
  {% if prefixCalo == prefixTau %}
    size_t ntau = 0;
  {% endif %}

  for (size_t ii = 0; ii < {{prefixCalo}}Bx.size(); ii++)
  {
    if (not ({{prefixCalo}}Bx.at(ii) == {{ calo.getBxOffset() }})) continue;
    {% if prefixCalo == prefixTau %}
      if (++ntau > {{maxTau}}) continue;
    {% endif %}
    {{ objects | hasEtaPhiCuts }}
    {{ macros.getObjectCuts(prefixCalo, 'ii', calo, tmEventSetup, nEtaBitsCalo) }}
    for (size_t jj = 0; jj < {{prefixMuon}}Bx.size(); jj++)
    {
      if (not ({{prefixMuon}}Bx.at(jj) == {{ muon.getBxOffset() }})) continue;
      {{ macros.getObjectCuts(prefixMuon, 'jj', muon, tmEventSetup, nEtaBitsMuon) }}
      {{ macros.getDifferentTypeCorrelationCuts(prefixCalo, prefixMuon, 'ii', 'jj', cond, tmEventSetup, LUTS, iPiMuon) }}
      pass = true;
      break;
    }
    if (pass) break;
  }

  return pass;
}
{% endblock CaloMuonCorrelationTemplate %}
{# eof #}
