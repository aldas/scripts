{#
 # @author: Takashi MATSUSHITA
 #}
{% block ObjectTemplate scoped %}
{% import 'macros.jinja2' as macros %}
{% set objects = cond.getObjects() %}
{% set nObjects = objects | length %}

{% if objects[0].getType() == tmEventSetup.Muon -%}
  {% set nEtaBits = scaleMap[tmGrammar.MU + '-' + tmGrammar.ETA].getNbits() -%}
  {% set prefix = prefixMuon %}
{% elif objects[0].getType() == tmEventSetup.Egamma -%}
  {% set nEtaBits = scaleMap[tmGrammar.EG + '-' + tmGrammar.ETA].getNbits() -%}
  {% set prefix = prefixEg %}
{% elif objects[0].getType() == tmEventSetup.Tau -%}
  {% set nEtaBits = scaleMap[tmGrammar.TAU + '-' + tmGrammar.ETA].getNbits() -%}
  {% set prefix = prefixTau %}
{% elif objects[0].getType() == tmEventSetup.Jet -%}
  {% set nEtaBits = scaleMap[tmGrammar.JET + '-' + tmGrammar.ETA].getNbits() -%}
  {% set prefix = prefixJet %}
{% endif -%}


bool
{{ cond.getName() }}
(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>& data)
{
  std::vector<int> candidates;
  for (size_t ii = 0; ii < {{prefix}}Bx.size(); ii++)
  {
    if (not ({{prefix}}Bx.at(ii) == {{ objects[0].getBxOffset() }})) continue;
    candidates.push_back(ii);
    {% if prefix == prefixTau %}
      if (candidates.size() == {{maxTau}}) break;
    {% endif %}
  }

  bool pass = false;
  if (candidates.size() < {{nObjects}}) return pass;

  std::vector<std::vector<int> > combination;
  getCombination(candidates.size(), {{nObjects}}, combination);
  std::vector<std::vector<int> > permutation;
  getPermutation({{nObjects}}, permutation);

  for (size_t ii = 0; ii < combination.size(); ii++)
  {
    const std::vector<int>& set = combination.at(ii);
    for (size_t jj = 0; jj < permutation.size(); jj++)
    {
      const std::vector<int>& indicies = permutation.at(jj);
      int idx = -1;
      {{ objects | hasEtaPhiCuts }}
      {% for kk in range(nObjects) %}
      idx = candidates.at(set.at(indicies.at({{kk}})));
      {{ macros.getObjectCuts(prefix, 'idx', objects[kk], tmEventSetup, nEtaBits) }}
      {% endfor %}
      {{ cond.getCuts() | chkChgCor(prefix, nObjects) }}
      pass = true;
      break;
    }

    if (pass) break;
  }

  return pass;
}
{% endblock ObjectTemplate %}
{# eof #}
