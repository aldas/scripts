{#
 # @author: Takashi MATSUSHITA
 #}
{% block EsumCorrelationTemplate scoped %}

{% import 'macros.jinja2' as macros %}

{% set objects = cond.getObjects() %}
{% set nObjects = objects | length %}
{% set object = objects[0] if objects[0].getType() in (tmEventSetup.Muon, tmEventSetup.Egamma, tmEventSetup.Tau, tmEventSetup.Jet) else objects[1] %}
{% set esum = objects[1] if objects[1].getType() in (tmEventSetup.ETM, tmEventSetup.HTM, tmEventSetup.ETMHF) else objects[0] %}

{% if object.getType() == tmEventSetup.Muon %}
  {% set prefix = prefixMuon %}
  {% set nEtaBitsObject = scaleMap[tmGrammar.MU + '-' + tmGrammar.ETA].getNbits() -%}
  {% set phiScale = scaleMap[tmGrammar.MU + '-' + tmGrammar.PHI] -%}
  {% if esum.getType() == tmEventSetup.ETM %}
    {% set LUTS = {'CONV_PHI': 'LUT_PHI_ETM2MU',
                   'DPHI': 'LUT_DPHI_MU_ETM'} %}
  {% elif esum.getType() == tmEventSetup.HTM %}
    {% set LUTS = {'CONV_PHI': 'LUT_PHI_HTM2MU',
                   'DPHI': 'LUT_DPHI_MU_HTM'} %}
  {% endif %}

{% elif object.getType() == tmEventSetup.Egamma %}
  {% set prefix = prefixEg %}
  {% set nEtaBitsObject = scaleMap[tmGrammar.EG + '-' + tmGrammar.ETA].getNbits() -%}
  {% if esum.getType() == tmEventSetup.ETM %}
  {% set phiScale = scaleMap[tmGrammar.ETM + '-' + tmGrammar.PHI] -%}
    {% set LUTS = {'DPHI': 'LUT_DPHI_EG_ETM'} %}
  {% elif esum.getType() == tmEventSetup.HTM %}
    {% set LUTS = {'DPHI': 'LUT_DPHI_EG_HTM'} %}
  {% endif %}

{% elif object.getType() == tmEventSetup.Jet %}
  {% set prefix = prefixJet %}
  {% set nEtaBitsObject = scaleMap[tmGrammar.JET + '-' + tmGrammar.ETA].getNbits() -%}
  {% set phiScale = scaleMap[tmGrammar.ETM + '-' + tmGrammar.PHI] -%}
  {% if esum.getType() == tmEventSetup.ETM %}
    {% set LUTS = {'DPHI': 'LUT_DPHI_JET_ETM'} %}
  {% elif esum.getType() == tmEventSetup.HTM %}
    {% set LUTS = {'DPHI': 'LUT_DPHI_JET_HTM'} %}
  {% endif %}

{% elif object.getType() == tmEventSetup.Tau %}
  {% set prefix = prefixTau %}
  {% set nEtaBitsObject = scaleMap[tmGrammar.TAU + '-' + tmGrammar.ETA].getNbits() -%}
  {% set phiScale = scaleMap[tmGrammar.ETM + '-' + tmGrammar.PHI] -%}
  {% if esum.getType() == tmEventSetup.ETM %}
    {% set LUTS = {'DPHI': 'LUT_DPHI_TAU_ETM'} %}
  {% elif esum.getType() == tmEventSetup.HTM %}
    {% set LUTS = {'DPHI': 'LUT_DPHI_TAU_HTM'} %}
  {% endif %}
{% endif %}

{% if esum.getType() == tmEventSetup.ETM -%}
  {% set type = 'L1Analysis::kMissingEt' %}
{% elif object.getType() == tmEventSetup.HTM -%}
  {% set type = 'L1Analysis::kMissingHt' %}
{% elif object.getType() == tmEventSetup.ETMHF -%}
  {% set type = 'L1Analysis::kMissingEtHF' %}
{% endif -%}

{% set iPi = (0.5*(phiScale.getMaximum() - phiScale.getMinimum())/phiScale.getStep()) | int -%}


bool
{{ cond.getName() }}
(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>& data)
{
  bool pass = false;
  {% if prefix == prefixTau %}
    size_t ntau = 0;
  {% endif %}

  {{ objects | hasEtaPhiCuts }}
  for (size_t ii = 0; ii < {{prefix}}Bx.size(); ii++)
  {
    if (not ({{prefix}}Bx.at(ii) == {{ object.getBxOffset() }})) continue;
    {% if prefix == prefixTau %}
      if (++ntau > {{maxTau}}) break;
    {% endif %}
    {{ macros.getObjectCuts(prefix, 'ii', object, tmEventSetup, nEtaBitsObject) }}

    for (size_t jj = 0; jj < {{prefixSum}}Bx.size(); jj++)
    {
      if (not ({{prefixSum}}Type.at(jj) == {{ type }})) continue;
      if (not ({{prefixSum}}Bx.at(jj) == {{ esum.getBxOffset() }})) continue;
      {{ macros.getEsumCuts(prefixSum, 'jj', esum, tmEventSetup) }}
      {{ macros.getEsumCorrelationCuts(prefix, prefixSum, 'ii', 'jj', cond, tmEventSetup, LUTS, iPi) }}
      pass = true;
      break;
    }

    if (pass) break;
  }

  return pass;
}
{% endblock EsumCorrelationTemplate %}
{# eof #}
