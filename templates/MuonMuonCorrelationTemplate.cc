{#
 # @author: Takashi MATSUSHITA
 #}
{% block MuonMuonCorrelationTemplate scoped %}

{% import 'macros.jinja2' as macros %}

{% set objects = cond.getObjects() %}
{% set nObjects = objects | length %}

{% set nEtaBits = scaleMap[tmGrammar.MU + '-' + tmGrammar.ETA].getNbits() -%}
{% set nPhiBits = scaleMap[tmGrammar.MU + '-' + tmGrammar.PHI].getNbits() -%}
{% set prefix = prefixMuon %}
{% set precision = 2*scaleMap['PRECISION-' + tmGrammar.MU + '-' + tmGrammar.MU + '-MassPt'].getNbits()
                 +   scaleMap['PRECISION-' + tmGrammar.MU + '-' + tmGrammar.MU + '-Math'].getNbits() %}
{% set LUTS = {'DPHI': 'LUT_DPHI_MU_MU',
               'DETA': 'LUT_DETA_MU_MU',
               'COS_DPHI': 'LUT_COS_DPHI_MU_MU',
               'COSH_DETA': 'LUT_COSH_DETA_MU_MU',
               'ET0': 'LUT_MU_ET',
               'ET1': 'LUT_MU_ET',
               'PREC_MASS': precision
              } %}
{% set phiScale = scaleMap[tmGrammar.MU + '-' + tmGrammar.PHI] -%}
{% set iPi = (0.5*(phiScale.getMaximum() - phiScale.getMinimum())/phiScale.getStep()) | int -%}


bool
{{ cond.getName() }}
(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>& data)
{
  std::vector<int> candidates;
  for (size_t ii = 0; ii < {{prefix}}Bx.size(); ii++)
  {
    if (not ({{prefix}}Bx.at(ii) == {{ objects[0].getBxOffset() }})) continue;
    candidates.push_back(ii);
  }

  bool pass = false;
  if (candidates.size() < {{nObjects}}) return pass;

  std::vector<std::vector<int> > combination;
  getCombination(candidates.size(), {{nObjects}}, combination);
  std::vector<std::vector<int> > permutation;
  getPermutation({{nObjects}}, permutation);

  for (size_t ii = 0; ii < combination.size(); ii++)
  {
    const std::vector<int>& set = combination.at(ii);
    for (size_t jj = 0; jj < permutation.size(); jj++)
    {
      const std::vector<int>& indicies = permutation.at(jj);
      {{ objects | hasEtaPhiCuts }}
      const int idx0 = candidates.at(set.at(indicies.at(0)));
      const int idx1 = candidates.at(set.at(indicies.at(1)));
      {{ macros.getObjectCuts(prefix, 'idx0', objects[0], tmEventSetup, nEtaBits) }}
      {{ macros.getObjectCuts(prefix, 'idx1', objects[1], tmEventSetup, nEtaBits) }}
      {{ macros.getSameTypeCorrelationCuts(prefix, 'idx0', 'idx1', cond, tmEventSetup, LUTS, iPi) }}
      pass = true;
      break;
    }

    if (pass) break;
  }

  return pass;
}
{% endblock MuonMuonCorrelationTemplate %}
{# eof #}
