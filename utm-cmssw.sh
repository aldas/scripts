#!/bin/bash

# specify the revision to check out from the cactus repository
revision=46131

# install xsd 3.1.0 for generating c++ xml interface
wget http://www.codesynthesis.com/download/xsd/3.1/linux-gnu/x86_64/xsd-3.1.0-1.x86_64.rpm
sudo yum remove xsd
sudo yum install xsd-3.1.0-1.x86_64.rpm

# check out
svn co -r $revision svn+ssh://svn.cern.ch/reps/cactus/trunk/cactusprojects/utm cactusprojects/utm
root=`pwd`

# delete unnecessary files
cd cactusprojects/utm
rm -rf tmEditor tmGui tmReporter tmVhdlProducer ts 00ReadMe.txt setup.sh doxygen.config tmXsd/doc

# apply patch
cp -pr /usr/include/xsd xsd
patch -p2 < tmXsd/xsd.patch

version_patch=version.patch
cat > $version_patch <<- EndOfPatch
--- tmEventSetup/tmEventSetup.cc
+++ tmEventSetup/tmEventSetup.cc
@@ -54,7 +54,7 @@ _getTriggerMenu(const tmtable::Menu& menu, const tmtable::Scal
 
   estm->setName(getValue(menu.menu, "name"));
   estm->setVersion(version);
-  estm->setComment(getValue(menu.menu, "comment"));
+  estm->setComment(getValue(menu.menu, "comment") + " : processed with UTM r${revision}");
   estm->setDatetime(getValue(menu.menu, "datetime"));
   estm->setFirmwareUuid(getValue(menu.menu, "uuid_firmware"));
   estm->setScaleSetName(getValue(scale.scaleSet, "name"));
EndOfPatch
patch -p0 < $version_patch
rm -f $version_patch

# generate xml interface
export XSDCXX=xsd
cd tmXsd/gen-xsd-type
make

# fix Makefile for CMSSW environment
cd $root/cactusprojects/utm
target="tmXsd/gen-xsd-type/Makefile"
mv $target $target.dist
awk 'BEGIN{flag=1}/NOT_CMSSW_BEGIN/{flag=0;next}/NOT_CMSSW_END/{flag=1;next}flag' $target.dist > $target
rm $target.dist

# create gzip'ed tar archive
cd $root/cactusprojects
find . -name ".svn" -exec rm -rf {} \;
tar cvfz utm-r$revision-xsd310-patch.tgz utm

#eof
